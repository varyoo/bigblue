const fs = require('fs')
const winston = require('winston')
const got = require('got')

const log = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json()
    ),
    level: 'info',
    transports: [
        /*new winston.transports.Console({
            timestamp: function() {
                return new Date().toISOString()
            }
        })*/
        new winston.transports.Http({
            port: 8080,
        })
    ]
})

const outFiles = {}

const write = (line, lang) => {
    if (lang === null || lang === undefined) {
        lang = 'null'
    }
    let file = outFiles[lang]
    if (file === undefined) {
        file = fs.createWriteStream(`out/mock/${lang}.txt`, {
            flags: 'a' // 'a' means appending (old data will be preserved)
        })
        outFiles[lang] = file
    }
    if (lang in outFiles) {
        outFiles[lang].write(line)
        outFiles[lang].write('\n')
    }
}

const consume = async (job, queue) => {
    const remaining = queue.count
    const total = queue.totalCount
    // get random output
    try {
        const { body } = await got('https://randomuser.me/api/')
        const user = JSON.parse(body).results[0]
        let output = JSON.stringify(user);
        log.info({
            name: `${user.name.first} ${user.name.last}`,
            job: job,
            remaining,
            total,
            output: output,
        })
        write(output, user.nat)
    } catch (e) {
        log.error(JSON.stringify(e))
    }
}

const main = async name => {
    const total = 12435654
    let i = total
    while (i--) {
        await new Promise(r => setTimeout(r, 1000))
        const job =
            Math.random()
                .toString(36)
                .substring(2, 15) +
            Math.random()
                .toString(36)
                .substring(2, 15)
        await consume(job, { count: i, totalCount: total })
    }
}

process.on('SIGTERM', () => {
    log.info('graceful shutdown initiated')
    Object.values(outFiles).forEach(l => l.end())
    log.info('shutting down')
})

main()
