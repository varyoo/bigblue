package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"sync"
)

type Message struct {
	Name      string `json:"name"`
	Job       string `json:"job"`
	Remaining int    `json:"remaining"`
	Total     int    `json:"total"`
	Output    string `json:"output"`
}

func (m Message) PrettyOutput() (template.HTML, error) {
	tmp := make(map[string]interface{})
	if err := json.Unmarshal([]byte(m.Output), &tmp); err != nil {
		return template.HTML(""), err
	}

	b, err := json.MarshalIndent(tmp, "<br/>", "<span class=\"tab\"></span>")
	return template.HTML(b), err
}

type Job struct {
	Message Message `json:"message"`
	Level   string  `json:"level"`
}

func (j Job) Valid() bool {
	return j.Message.Job != ""
}

type Data struct {
	jobs []Job
	lock sync.RWMutex
}

func (d *Data) Push(j Job) {
	d.lock.Lock()
	defer d.lock.Unlock()

	for i := 8; i >= 0; i-- {
		d.jobs[i+1] = d.jobs[i]
	}

	d.jobs[0] = j
}

func (d *Data) Jobs() []Job {
	d.lock.RLock()
	defer d.lock.RUnlock()

	js := make([]Job, len(d.jobs))
	copy(js, d.jobs)
	return js
}

var data Data = Data{jobs: make([]Job, 10)}

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		j := Job{}

		if err := json.NewDecoder(r.Body).Decode(&j); err != nil {
			log.Println(err)
			return
		}

		data.Push(j)
	})

	http.HandleFunc("/monitor", func(w http.ResponseWriter, r *http.Request) {
		t, err := template.ParseFiles("monitor.html")
		if err != nil {
			log.Println(err)
			return
		}

		js := data.Jobs()
		var last Job

		if len(js) > 0 {
			last = js[0]
		}

		if err := t.ExecuteTemplate(w, "root", struct {
			Jobs []Job
			Last Job
		}{
			js,
			last,
		}); err != nil {
			log.Println(err)
		}
	})

	fmt.Println("ready")
	http.ListenAndServe(":8080", nil)
}
